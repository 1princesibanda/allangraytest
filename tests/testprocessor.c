#include <stddef.h>
#include <tap/basic.h>
#include <stdlib.h>
#include "processor.h"

bool canInsertSingleUser()
{
	struct y * user = (struct y*)malloc(sizeof(struct y));
	user->user = "testUser";
	user->type = USER;
	user->next = NULL;
	insertItem(user);
	return true;
}

bool canInsertSingleTweet()
{
	struct y * tweet = (struct x*)malloc(sizeof(struct x));
	tweet->user = "testUser";
	tweet->type = TWEET;
	tweet->message = "this is a test tweet message\n";
	insertItem(tweet);
	return true;
}

int main(void)
{
	plan(2);
	processorInit();
	ok(canInsertSingleUser(), "The processor can process a single user");
	ok(canInsertSingleTweet(), "The processor can process a single tweet message");

	/*
	plan(4);
	ok(1, "the first test");
	is_int(42, 42, NULL);
	diag("a diagnostic, ignored by the harness");
	ok(0, "a failing test");
	skip("a skipped test");
	*/
	return 0;
}
