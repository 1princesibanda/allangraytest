#include "main.h"
#include <string.h>

#define INITIALSIZE 10
#define NEXTINDEXINCREMENT 1

typedef enum { USER, TWEET} TYPE  ;

struct  x
{
	TYPE type;
	char * user;
	char * message;
};

struct y
{
	TYPE type; 
	char * user;
	struct y * next; 
};
