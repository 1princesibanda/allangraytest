/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_INCLUDES_BISONHEADER_H_INCLUDED
# define YY_YY_INCLUDES_BISONHEADER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ENDFILE = 258,
    NEWLINE = 259,
    FOLLOWS = 260,
    USERCHAR = 261,
    COMMA = 262,
    GREATERSPACE = 263,
    TWEETCHAR = 264,
    REJECT_ = 265
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{

  /* ENDFILE  */
  char * ENDFILE;
  /* NEWLINE  */
  char * NEWLINE;
  /* FOLLOWS  */
  char * FOLLOWS;
  /* USERCHAR  */
  char * USERCHAR;
  /* COMMA  */
  char * COMMA;
  /* GREATERSPACE  */
  char * GREATERSPACE;
  /* TWEETCHAR  */
  char * TWEETCHAR;
  /* REJECT_  */
  char * REJECT_;
  /* file  */
  char * file;
  /* userfile  */
  char * userfile;
  /* userfilelines  */
  char * userfilelines;
  /* userfileline  */
  char * userfileline;
  /* user  */
  char * user;
  /* tail  */
  char * tail;
  /* userlist  */
  char * userlist;
  /* tweetfile  */
  char * tweetfile;
  /* tweetfilelines  */
  char * tweetfilelines;
  /* tweetfileline  */
  char * tweetfileline;
  /* tweetMsg  */
  char * tweetMsg;
#line 104 "includes/bisonHeader.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif



int yyparse (void);

#endif /* !YY_YY_INCLUDES_BISONHEADER_H_INCLUDED  */
