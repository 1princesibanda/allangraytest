#include <argp.h>
#include <stdio.h>
#include <errno.h> //GNU error code like ENOENT 'no such file or directory'. Can be used with parser()
#include <stdlib.h>
#include <assert.h>

#ifdef PARSER
	#include "bisonHeader.h" //provide --defines=FILE to flex to output header flexHeader.h into the includes dir
#endif

#ifdef LEXER
	#include "flexHeader.h" //provide -header-file=FILE to flex to output header flexHeader.h into the includes dir
#endif

#define SUCCESS 0
#define FAILURE 1

/*
 * EXIT CODES:
 * 0 Execution with no errors
 * 1 Incorrect command line input arguments
 * 2 Could not open userfile.
 */

typedef enum { false, true } bool;

extern bool verbose;
