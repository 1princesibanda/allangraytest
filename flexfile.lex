
%{
#include <ctype.h>
#include <math.h>
#include <string.h> 
#include <stdio.h>
#include "bisonHeader.h"

static int multiple = 1;
static int accumSize = YYLMAX;
static int accumAvailable = YYLMAX;
static char * accum = "\0";

%}
%x follows user comma greaterSpace tweetMessage
%array
%option noyywrap
%option nodefault 
%option warn
	/* ****************************RE-ENABLE BISON OPTIONS WHEN WRITTEN THE BISON SCRIPT******************************* */
%option bison-bridge
%option bison-locations

%top{
}

FOLLOWS follows
COMMA ,
NEWLINE \n|(\r\n)
TWEETCHAR [^\n]
GREATERSPACE [>][ ]
USERCHAR [^,\n]

%%
%{
static int flag = 0;
enum whichfile { userfile, tweeterfile };

%}
	/* Matched input is made available on yytext and its length on yyleng */
	/* Remember where multiple regex match, the regex with longest sequence takes precedence */
<INITIAL><<EOF>>			{ 
						BEGIN(INITIAL);
						yylval->ENDFILE = strdup(yytext);
						return ENDFILE;
					}

<INITIAL,tweetMessage>{NEWLINE} 	{ 
						BEGIN(INITIAL);
						yylval->NEWLINE = strdup(yytext);
						return NEWLINE;
					}

<INITIAL>{FOLLOWS} 			{ 
						yylval->FOLLOWS = strdup(yytext);
						return FOLLOWS;
					}
<INITIAL>{USERCHAR}			{
						yylval->USERCHAR = strdup(yytext);
						return USERCHAR;
		}

<INITIAL>{COMMA}			{ 
						yylval->COMMA = strdup(yytext);
						return COMMA;
					}

<INITIAL>{GREATERSPACE}			{ 
						BEGIN(tweetMessage);
						yylval->GREATERSPACE = strdup(yytext);
						return GREATERSPACE;
					}

	/* Limit to 140 chars in bison */
<tweetMessage>{TWEETCHAR}		{ 
						yylval->TWEETCHAR = strdup(yytext);
						return TWEETCHAR;
					}

<*>.|\n					{
						yylval->REJECT_ = strdup(yytext);
						return REJECT_;
					}

%%

/*
int main(int argc, char* argv)
{
	printf("Hello\n");
}
*/
