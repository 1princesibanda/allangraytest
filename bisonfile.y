%code top {
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "processor.h"
char string[400];
#define STRFORMARTER "%s"
#define MAXMESSAGELENGTH 140
}

/*%glr-parser*/

%locations
%define api.pure full
%define api.value.type union
%define parse.trace
//%printer { printf(yyoutput, "%s", ((char *)$$)); } <char *>;

%code {
void yyerror(YYLTYPE * locp, char const * msg);
int yylex(YYSTYPE* yylval_param,YYLTYPE* yyloc_param);
void reverseString(char * s);
//int yydebug = 1;
size_t linesize = 0;
char * runningStr = NULL;
size_t strIndex = 0;
}

%token <char *> ENDFILE NEWLINE FOLLOWS USERCHAR COMMA GREATERSPACE TWEETCHAR REJECT_

/* %left <char *> ENDLINE */


/* Enumerate all non-terminals */
%type <char *> file userfile tweetfile userfilelines userfileline user userlist tail tweetMsg tweetfilelines tweetfileline

/*
%precedence SHIFTMULTILINES
*/

%%
				/* USERFILE and TWEETFILE GRAMMARS */

file: userfile ENDFILE 
     		{
			$$ = $1;
			free($1);
YYACCEPT ;
		}

userfile: userfilelines 
		{ 
			$$ = $1;
		}

userfilelines: userfileline NEWLINE 
     		{
			size_t s1 = strlen($1), s2 = strlen($2);
			linesize = s1+s2;
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			$$ = accumulation;
			free($1);
		}
userfilelines: userfileline NEWLINE userfilelines
     		{
			size_t s1 = strlen($1), s2 = strlen($2), s3 = strlen($3);
			linesize = s1+s2+s3;
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			strcat(&(accumulation[s1+s2]),$3);
			$$ = accumulation;
			free($1);
		}

userfileline: user FOLLOWS userlist
     		{
			size_t s1 = strlen($1), s2 = strlen($2), s3 = strlen($3);
			linesize = s1+s2+s3;
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			strcat(&(accumulation[s1+s2]),$3);
			
			$$ = accumulation;
			char * list = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(list, $3);

			char * tok = strtok(list,"#");
			struct y * head = (struct y *)malloc(sizeof(struct y));
			char * headName = (char *)malloc(sizeof(char)*(s1+1));
			strcpy(headName, $1);
			head->user = headName;
			head->next = NULL;
			struct y * prev = head;
			while( tok != NULL )
			{
				s3 = strlen(tok);
				struct y * followee = (struct y *)malloc(sizeof(struct y));
				char * followeeName = (char *)malloc(sizeof(char)*(s3+1));
				strcpy(followeeName , tok);
				followee->user = followeeName;
				followee->next = NULL;
				assert( prev != NULL);
				prev->next = followee;
				prev = followee;
				tok = strtok(NULL,"#");

			}
				insertItem(head, USER);
		}

user: USERCHAR tail
     		{
			if( $2 == NULL)
			{
				$$ = $1;
			}
			else
			{
				size_t s1 = strlen($1), s2 = strlen($2);
				char * cat = (char*)malloc(sizeof(char)*(s1+s2+1)); //2 is char in $1 and its \0
				if( cat != NULL )
				{
					strcpy(cat, $1);
					strcat(cat,$2);
				$$ = cat;
				}
				else
				{
					printf("Out of memory.\n");
					exit(1);
				}

			}

		}

tail: %empty 
     		{
			$$ = NULL;
		}
tail: user
     		{
			$$ = $1;
		}

userlist: user
     		{
			$$ = $1;
		}
userlist: user COMMA userlist
				{
					size_t s1 = strlen($1), s2 = strlen($3);
					linesize = s1+s2+1;
					char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
					strcpy(accumulation, $1);
					accumulation[s1] = '#';
					strcat(&(accumulation[s1+1]),$3);
					$$ = accumulation;
				}





file: tweetfile ENDFILE
     		{
YYACCEPT;
			size_t s1 = strlen($1), s2 = strlen($2);
			linesize = s1+s2;
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			$$ = accumulation;
		}

tweetfile: tweetfilelines 
     		{
			$$ = $1;
		}

tweetfilelines: tweetfileline NEWLINE 
	      	{
			size_t s1 = strlen($1), s2 = strlen($2);
			linesize = s1+s2;
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			$$ = accumulation;
		}

tweetfilelines: tweetfileline NEWLINE tweetfilelines
	      	{
			size_t s1 = strlen($1), s2 = strlen($2), s3 = strlen($3);
			linesize = s1+s2+s3;
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			strcat(&(accumulation[s1+s2]),$3);
			$$ = accumulation;
		}

tweetfileline: user GREATERSPACE tweetMsg
     		{
			size_t s1 = strlen($1), s2 = strlen($2), s3 = strlen($3);
			linesize = s1+s2+s3;
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			strcat(&(accumulation[s1+s2]),$3);
			$$ = accumulation;

			struct x * message = (struct x*)malloc(sizeof(struct x));
			char * userCpy = (char *)malloc(sizeof(char)*(strlen($1)+1));
			char * messageCpy = (char *)malloc(sizeof(char)*(strlen($3)+1));
			message->user = strcpy(userCpy, $1);
			message->message = strcpy(messageCpy, $3);
			insertItem(message, TWEET);
		}

	/* Please limit to only 140 chars */
tweetMsg: TWEETCHAR tweetMsg
     		{
			size_t s1 = strlen($1), s2 = strlen($2);
			linesize = s1+s2;
			if(s2 > MAXMESSAGELENGTH)
			{
				printf("Tweet Message has more than 140 characters. Aborting. \n");
				exit(5);
			}
			char * accumulation = (char *)malloc(sizeof(char)*(linesize+1));
			strcpy(accumulation, $1);
			strcat(&(accumulation[s1]),$2);
			$$ = accumulation;
		}
tweetMsg: %empty
	{
		$$ = "\0";
	}


%%


void yyerror(YYLTYPE * locp, char const * msg)
{
	printf("Error detected by parser.\n");
	//printf("%s:%d:%d: Out of Memory Error:%s\n", filename, locp->last_line, locp->last_column, msg);
}

void reverseString(char * s)
{
	int c, i, j;
	for(i = 0, j = strlen(s)-1; i < j; i++, j--)
	{
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}

}

