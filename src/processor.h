#include "main.h"
#include <string.h>

typedef enum { USER, TWEET} TYPE  ;

struct  x
{
	TYPE type;
	char * user;
	char * message;
};

struct y
{
	TYPE type; 
	char * user;
	struct y * next; 
};

void processorInit();
void insertItem(void * item, TYPE type);
void display();
