#include <argp.h>
#include <stdio.h>
#include <errno.h> //GNU error code like ENOENT 'no such file or directory'. Can be used with parser()
#include <stdlib.h>
#include "bisonHeader.h" //provide --defines=FILE to flex to output header flexHeader.h into the includes dir
#include "flexHeader.h" //provide -header-file=FILE to flex to output header flexHeader.h into the includes dir
#include "processor.h"

/*
 * EXIT CODES:
 * 0 Execution with no errors
 * 1 Incorrect command line input arguments
 * 2 Could not open userfile.
 */

static FILE * tweeterfile = NULL;
bool verbose = false;

struct arguments
{
	char * args[2];
	int silent, verbose;
	char * outputFile;
};

static error_t parser(int key, char * arg, struct argp_state * state)
{
	struct arguments * defaults = state->input;
	switch(key)
	{
		case 'v':
			verbose = true;
			printf("Verbose option set.\n");
			break;
		case 'u':
			if( verbose )
				printf("Got value for userfile:%s\n",arg);
			yyin = fopen(arg, "r");
			if (yyin)
			{
				if( verbose )
					printf("Successfully managed to open userfile.\n");
			}
			else 
			{
				if( verbose )
					printf("Could not open userfile. Aborting...\n");
				exit(2);
			}
			break;
		case 't':
			if( verbose )
				printf("Got value for tweeterfile:%s\n",arg);
			tweeterfile = fopen(arg, "r");
			if (tweeterfile)
			{
				if( verbose )
					printf("Successfully managed to open tweeterfile.\n");
			}
			else
			{
				if( verbose )
					printf("Could not open tweeterfile. Aborting...\n");
				exit(2);
			}
			break;
		case ARGP_KEY_ARG:
			printf("Got key arg\n");
			break;
		case ARGP_KEY_END:
			if ( (yyin == NULL) || (tweeterfile == NULL) )
			{
				printf("Exiting, because error on at least one file\n");
				argp_usage(state);
			}
			break;
		default:
			{
				return ARGP_ERR_UNKNOWN;
			}
	}
	return 0;
}


static void handleParsing(void)
{
	switch( yyparse() )
	{
		case 0:
			if( verbose )
				printf("Successful parsing.\n");
			break;
		case 1:
			printf("File syntax error.\n");
			break;
		case 2:
			printf("Parser memory exhausted.\n");
			break;
		default:
			printf("Unknown error.\n");
	}
}

int main(int argc, char* argv[])
{
	const char * argp_program_version = "1.0";
	const char * argp_program_bug_address = "1.0";

	static struct arguments defaultValues;
	defaultValues.silent = 0;
	defaultValues.verbose = 0;
	defaultValues.outputFile = "-";

	static char usage_doc[] = "-u pathToUserFile -t pathToTweeterFile";
	static char program_description[] = "This program simulates a twitter like feed.";
	static const struct argp_option options[] =	{
							{ "verbose", 'v', NULL, OPTION_ARG_OPTIONAL, "Set verbose on.", 0 },
							{ "userfile", 'u', "FILE", 0, "The full path to a file describing tweeter users and followers.", 0 },
							{ "tweeterfile", 't', "FILE", 0, "The full path to a file providing tweeters and tweet messages.", 0 },
							{ 0 }
							};
	static const struct argp argp = { options, parser, usage_doc, program_description };

	argp_parse(&argp, argc, argv, ARGP_IN_ORDER, NULL, &defaultValues);

	processorInit();
	if( verbose )
		printf("***************************************User file.*****************************************\n");
	handleParsing();
	fclose(yyin);

	if( verbose )
		printf("***************************************Switch to tweeter file.*****************************************\n");
	yyin = tweeterfile;
	handleParsing();
	fclose(yyin);

	display();
	return 0;
}
