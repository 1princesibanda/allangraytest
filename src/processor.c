#include "processor.h"

#define INITIALSIZE 10
#define NEXTINDEXINCREMENT 1

static size_t userSize = INITIALSIZE, userIndex = 0, tweetIndex = 0, tweetSize = INITIALSIZE;

extern bool verbose;

static struct x** tweetsArr;
static struct y** usernamesArr;
static void insert( void* item, TYPE type);

void processorInit()
{
	usernamesArr = (struct y **)malloc(sizeof(struct y*)*INITIALSIZE);
	tweetsArr = (struct x **)malloc(sizeof(struct x*)*INITIALSIZE);
	if( (usernamesArr == NULL) || (tweetsArr == NULL) )
	{
		printf("Failed to obtain memory.\n");
		exit(4); 
	}

	int i;
	for(i = 0; i < INITIALSIZE; i++)
	{
		usernamesArr[i] = NULL;
		tweetsArr[i] = NULL;
	}
}

//selection sort
static void sortNames()
{
	if( usernamesArr == NULL )
	{
		return ;
	}
	else
	{
		int i, j;
		struct y * tmp = NULL;
		for(i = 0; i < userIndex-1; i++)
		{
			for(j = i+1; j < userIndex; j++)
			{
				if( strcmp(usernamesArr[i]->user, usernamesArr[j]->user) > 0 )
				{
					tmp = usernamesArr[i];
					usernamesArr[i] = usernamesArr[j];
					usernamesArr[j] = tmp;
				}
			}
		}
	}
}

void insertImpliedUser( struct y * follower)
{
	assert( follower != NULL); //We expect insertImplied to be called on existing(ie non NULL) users
	assert( follower->type == USER);
	//This follower has at least one person they follow, otherwise they just a followee user only, which this function can find but should not get as input 
	assert( follower->next != NULL); 
	struct y* next = follower->next;
	size_t i;
	do
	{
		for( i = 0; i < userIndex; i++)
		{
			if( strcmp(next->user, usernamesArr[i]->user ) == 0 )
			{
				break;
			}
		}

		if( i == userIndex )
		{
			struct y* item = (struct y*)malloc(sizeof(struct y));
			size_t namelen = strlen(next->user);
			char * username = (char*)malloc(sizeof(char)*(namelen+1));
			strcpy(username, next->user);
			username[namelen] = '\0';
			item->type = USER;
			item ->user = username;
			item->next = NULL;
			insert(item, USER);
		}

		next = next->next;
	}
	while( next != NULL);

}

void insertItem( void* item, TYPE type)
{
	insert(item, type);
	if(type == USER)
		insertImpliedUser((struct y*)item);
}

void insert( void* item, TYPE type)
{

	size_t * index, * size;

	if( item == NULL)
	{
		printf( "Attempt to add a null item. Aborting.\n" );
		exit(5); 
	}
	else
	{
		switch(type) 
		{
			case USER:
				index = &(userIndex);
				size = &(userSize);
				break;
			case TWEET:
				index = &(tweetIndex);
				size = &(tweetSize);
				break;
			default:
				printf( "Unkown item type. \n" );
				exit(6);
				break;
		}
	}

	if( (*index) > (*size)-NEXTINDEXINCREMENT )
	{
		assert((*index) == (*size));
		if( verbose )
			printf("Out of user data memory- requesting more memory...");

		static void ** biggerArr = NULL;
		if( type == USER)
		{
			biggerArr = realloc(*usernamesArr, sizeof(struct y*) * ( (*size)+INITIALSIZE) );
		}
		else
		{
			assert(type == TWEET);
			biggerArr = realloc(*tweetsArr, sizeof(struct x*) * ( (*size)+INITIALSIZE) );
		}

		if( biggerArr != NULL )
		{
			if( verbose )
				printf("Success.\n");
			(*size) += INITIALSIZE;
		}
		else
		{
			printf("Denied.\n");
			exit(4); //We should keep a header of exit codes (say, constants) shared by all program parts
		}
	}

	if( type == USER ) //Its a user, so add to list of followers if user already exists, otherwise create new user.
	{
		size_t i = 0; 
		if( usernamesArr[i] == NULL )//array empty, so item can be the first entry
		{
			usernamesArr[i] = item;
			(*index) += NEXTINDEXINCREMENT; 
		}
		else
		{
			for( i = 0; i < (*index); i++)
			{

				if( strcmp(usernamesArr[i]->user, ((struct y*)item)->user) == 0 )
				{
					struct y* next = usernamesArr[i];
					while( next->next != NULL)
						next = next->next;
					next->next = ((struct y*)item);
					break;
				}
			}
			if( i == (*index) ) //We did not manage to find an already existing user, so we add item as the last user
			{
				usernamesArr[i] = item;
				(*index) += NEXTINDEXINCREMENT; 
			}
		}
	}
	else //Its a tweet message, so it just needs to join the end of the chronologically ordered array.
	{
		assert(type == TWEET);
		tweetsArr[(*index)] = item;
		(*index) += NEXTINDEXINCREMENT; 
		free(&(((struct x*)item)->type));
	}
}

/*
static void insertUser(struct x* user)
{
	insertItem(usernamesArr, tweetIndex, tweetSize, user);
}

static void insertLine(struct y* line)
{
	insertItem(tweetsArr, index, size, line);
}
*/

/* This will display the users and messages in format requested */
void display()
{
	static size_t i = 0, j = 0;
	struct y* next = NULL;
	sortNames();
	if( tweetSize == 0 )
	{
		for( ; i < userIndex; i++ )
		{
			printf("%s\n",usernamesArr[i]->user);
		}
	}
	else
	{
		for( ; i < userIndex; i++ )
		{
			printf("%s\n",usernamesArr[i]->user);
			for( j = 0; j < tweetIndex; j++ )
			{
				next = usernamesArr[i];
				while( next != NULL)
				{
					fflush(stdout);
					if( strcmp(next->user, tweetsArr[j]->user) == 0 )
					{
						printf( "\t@%s: %s\n", tweetsArr[j]->user, tweetsArr[j]->message );
						break;
					}
					else
					{
						if(verbose)
							printf("Negatively compared '%s' and '%s'\n",next->user, tweetsArr[j]->user);
					}
					next = next->next;

				}
			}

		}
	}



}
